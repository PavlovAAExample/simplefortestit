""" Navigation Menu """
import time

from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By

from Components.base_component import BaseComponent


class NavMenu(BaseComponent):
    """ Navigation Menu """

    MENU = "div.item"
    SUBMENU = "div.wrapper a"

    def __init__(self, browser, input_point):
        super().__init__(browser, input_point)
        self.locator_menu = input_point + self.MENU

    def hover_to_menu(self, number):
        self.hover_to_locator(self.locator_menu + f":nth-child({number+1})")
        time.sleep(1)

    def get_menu_amount(self):
        return self.get_elements_count(self.locator_menu)

    def get_list_hover_color_in_submenu(self, menu_number):
        color_list = []
        locator = self.locator_menu + f":nth-child({menu_number+1}) " + self.SUBMENU
        elements = self.browser.find_elements(By.CSS_SELECTOR, locator)

        for element in elements:
            hover = ActionChains(self.browser).move_to_element(element)
            hover.perform()
            color_list.append(element.value_of_css_property('background-color'))

        return color_list

