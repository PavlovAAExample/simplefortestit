""" Базовые методы. """
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver import ActionChains


class BaseComponent:
    """ Реализует базовые методы работы с элементами страниц. """

    PROGRESS_BAR = "//mat-progress-bar"

    def __init__(self, browser, input_point=None, timeout: int = 10):
        self.browser = browser
        self.entry = input_point
        self.browser.implicitly_wait(timeout)

    def hover_to_locator(self, what: str):
        """ """
        element = self.browser.find_element(By.CSS_SELECTOR, what)
        hover = ActionChains(self.browser).move_to_element(element)
        hover.perform()

    def get_elements_count(self, what: str):
        """
        Сосчитать количество элементов с определенным локатором. \n
        :param what: локатор элемента
        :return: количество элементов
        """
        elements = self.browser.find_elements(By.CSS_SELECTOR, what)
        elements_count = len(elements)

        return elements_count

    def wait_until_clickable(self, what: str, timeout: int = 15):
        """
        Дождаться, пока элемент не станет кликабельным. \n
        :param what: локатор элемента
        :param timeout: секунды ожидания
        """
        locator = (By.CSS_SELECTOR, what)
        WebDriverWait(self.browser, timeout).until(EC.element_to_be_clickable(locator))
