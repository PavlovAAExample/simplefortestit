"""Tests first feature"""
import pytest
import pytest_check as check
import autorization
import testit
import API.crud_events as events

from pages.home_page import HomePage


class TestFeatureOne:

    @pytest.fixture(autouse=True)
    def setup(self, browser):
        self.home_page = HomePage(browser)
        print("basic setup")

    def setup_test_hover_color_in_nav_menu(self):
        events.create()
        autorization.authorization_with_user(self)

    @testit.displayName('displayName1')
    @testit.externalID('externalID1')
    def test_hover_color_in_nav_menu(self):
        color = "rgba(89, 164, 23, 1)"
        color_on_page = []
        self.home_page.open_page()
        for menu_number in range(self.home_page.nav_menu.get_menu_amount()):
            self.home_page.nav_menu.hover_to_menu(menu_number)
            color_on_page.extend(self.home_page.nav_menu.get_list_hover_color_in_submenu(menu_number))

        for y in color_on_page:
            check.is_true(y == color)

    def teardown_test_hover_color_in_nav_menu(self):
        events.delete()

    @testit.displayName('displayName2')
    @testit.externalID('externalID2')
    def test_pass(self):
        print(f"Test for Pass")
        assert True

    @testit.displayName('displayName3')
    @testit.externalID('externalID3')
    def test_fail(self):
        print(f"Test for Fail")
        assert False
