import pytest
import testit
import time


def setup_module():
	with testit.step('unittest module step 1'):
		pass

@testit.workItemID(20)
@testit.displayName("Тест 6 - ({value_type})")
@testit.externalID("{value_id}")
@testit.link(url='{url}', title='This is {titleLink}')
@testit.labels('{labels}')
@testit.title('{title}')
@pytest.mark.parametrize('value_type, value_id, Assert, labels, title, url, titleLink', [
	("FLOAT", 'ext_float', True, ('area', 'test'), 'name1', 'https://dumps.example.com/module/some_module_dump', 'AbraCadabra'),
	("LONG", 'ext_long', True, 'area', 'name2', 'https://vk.com', 'CadabraAbra'),
	("STRING", 'ext_string', True, ('Area12', 'chto-to'), 'name3', 'https://ok.ru', 'PROtest'),
	("BOOL", 'ext_bool', True, [], 'name4', 'https://google.com', 'GOOOOOOOOGLE'),
	("DOUBLE", 'ext_double', True, '23', 'name5','https://youtube.com', 'Русская локализация')
])
def test_6(value_type, value_id, Assert, labels, title, url, titleLink):
	testit.addLink(title='component_dump.dmp', type=testit.linkType.RELATED, url='https://dumps.example.com/module/some_module_dump')
	testit.addLink(type=testit.linkType.BLOCKED_BY, url='https://dumps.example.com/module/some_module_dump')
	testit.addLink(type=testit.linkType.DEFECT, url='https://dumps.example.com/module/some_module_dump')
	testit.addLink(type=testit.linkType.ISSUE, url='https://dumps.example.com/module/some_module_dump')
	testit.addLink(type=testit.linkType.REQUIREMENT, url='https://dumps.example.com/module/some_module_dump')
	testit.addLink(type=testit.linkType.REPOSITORY, url='https://demo.testit.software/projects/4/autotests/test-runs/e267a03b-fa03-41ba-9812-c803c828237f')
	with testit.step('step 1'):
		with testit.step('step 1.1'):
			with testit.step('step 1.1.1'):
				#time.sleep(2)
				assert True
		with testit.step('step 1.2'):
			with testit.step('step 1.2.1'):
				assert True
			with testit.step('step 1.2.2'):
				assert True
	with testit.step('step 2'):
		#time.sleep(4)
		assert Assert
	with testit.step('step 3'):
		assert True
	with testit.step('step 4'):
		assert True

@testit.externalID('Номер теста 71')
@testit.displayName('name')
@pytest.mark.skip
def test_7():
	#time.sleep(13)
	assert True
	assert True

class Test_34:
	@testit.displayName('displayName8')
	@testit.externalID('externalID8')
	def test_8(self):
		#time.sleep(5)
		assert False
