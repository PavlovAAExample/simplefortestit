"""Authorization methods"""


def authorization_with_user(self):
    print("Successful user authorization")


def authorization_with_admin(self):
    print("Successful admin authorization")
