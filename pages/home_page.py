""" HomePage """
import time

from Components.base_component import BaseComponent
from Components.nav_menu import NavMenu


class HomePage(BaseComponent):
    """ HomePage """

    URL = "https://www.zyfra.com"
    INPUT_POINT = "div.page "
    NAVIGATION = "nav.nav "

    def __init__(self, browser, input_point=INPUT_POINT):
        super().__init__(browser, input_point)
        self.browser = browser
        self.nav_menu = NavMenu(browser, self.NAVIGATION)

    def open_page(self):
        self.browser.get(self.URL)
        self.wait_navigation()
        time.sleep(4)

    def wait_navigation(self):
        self.wait_until_clickable(self.NAVIGATION)


