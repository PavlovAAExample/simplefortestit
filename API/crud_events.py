import time
import pytest


def create():
    time.sleep(1)
    print("Create method was successful")


def read():
    time.sleep(1)
    print("Read method was successful")


def update():
    print("Update method was successful")


def delete():
    time.sleep(1)
    pytest.raises(ConnectionError)
